package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testContainsObstacleAtX4Y7ShouldReturnTrue() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void testContainsObstacleAtX2Y3ShouldReturnTrue() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(rover.planetContainsObstacleAt(2, 3));
	}
	
	@Test
	public void testEmptyCommandStringShouldReturnLandingStatus() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "";
		
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
	}
	
	@Test
	public void testOnlyRCommandShouldReturn00E() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "r";
		
		assertEquals("(0,0,E)", rover.executeCommand(commandString));
	}
	
	@Test
	public void testOnlyLCommandShouldReturn00W() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "l";
		
		assertEquals("(0,0,W)", rover.executeCommand(commandString));
	}
	
	@Test
	public void testOnlyFCommandShouldReturn01N() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f";
		
		assertEquals("(0,1,N)", rover.executeCommand(commandString));
	}
	
	@Test
	public void testBCommandFromX5Y8DirectionEShouldReturn48E() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		
		rover.setPosition(5, 8, "E");
		
		assertEquals("(4,8,E)", rover.executeCommand(commandString));
	}
	
	@Test
	public void testFFRFFCommandFromLandingStatusShouldReturn22E() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrff";
		
		assertEquals("(2,2,E)", rover.executeCommand(commandString));
	}
	
	@Test
	public void testBCommandFromLandingStatusShouldReturn09N() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		
		assertEquals("(0,9,N)", rover.executeCommand(commandString));
	}

}
