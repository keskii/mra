package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	int[][] planet;
	int currentRowMatrix;
	int currentColumnMatrix;
	int currentXplanet;
	int currentYplanet;
	String currentDirection;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		// To be implemented
		int x;
		int y;
		planet = new int[planetX][planetY];

		for(int r = 0; r < planetX; r++) {
			for(int c = 0; c < planetY; c++) {
				planet[r][c] = 0;
			}
		}
		
		for(int i = 0; i < planetObstacles.size(); i++) {
			x = Integer.parseInt(planetObstacles.get(i).substring(planetObstacles.get(i).indexOf("(") + 1, planetObstacles.get(i).indexOf(",")));
			y = Integer.parseInt(planetObstacles.get(i).substring(planetObstacles.get(i).indexOf(",") + 1, planetObstacles.get(i).indexOf(")")));
			
			planet[planetX - 1 - y][x] = 1;
		}
		
		planet[planetX - 1][0] = 2;
		currentRowMatrix = planetX - 1;
		currentColumnMatrix = 0;
		currentXplanet = 0;
		currentYplanet = 0;
		currentDirection = "N";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		// To be implemented
		if(planet[planet.length - 1 - y][x] == 1)
			return true;
		else
			return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		// To be implemented
		String status = null;
		if(commandString != "") {
			if(commandString.length() > 1) {
				moreCommands(commandString);
			} else {
				singleCommand(commandString);
			}
			status = "(" + Integer.toString(currentXplanet) + "," + Integer.toString(currentYplanet) + "," + currentDirection + ")";
		} else {
			status = "(0,0,N)";
		}
		
		return status;
	}
	
	private void turnRight() {
		if(currentDirection == "N") {
			currentDirection = "E";
		} else if(currentDirection == "E") {
			currentDirection = "S";
		} else if(currentDirection == "S") {
			currentDirection = "W";
		} else currentDirection = "N";
	}
	
	private void turnLeft() {
		if(currentDirection == "N") {
			currentDirection = "W";
		} else if(currentDirection == "W") {
			currentDirection = "S";
		} else if(currentDirection == "S") {
			currentDirection = "E";
		} else currentDirection = "N";
	}
	
	private void moveForward() {
		if(currentDirection == "N") {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentYplanet++;
			currentRowMatrix--;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		} else if(currentDirection == "E") {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentXplanet++;
			currentColumnMatrix++;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		} else if(currentDirection == "S") {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentYplanet--;
			currentRowMatrix++;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		} else {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentXplanet--;
			currentColumnMatrix--;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		}
	}
	
	private void moveBack() {
		if(currentDirection == "N") {
			directionNorthMoveBackOperations();
		} else if(currentDirection == "E") {
			directionEastMoveBackOperations();
		} else if(currentDirection == "S") {
			directionSouthMoveBackOperations();
		} else {
			directionWestMoveBackOperations();
		}
	}
	
	public void setPosition(int x, int y, String direction) {
		planet[currentRowMatrix][currentColumnMatrix] = 0;
		currentYplanet = y;
		currentXplanet = x;
		currentRowMatrix = planet.length - 1 - y;
		currentColumnMatrix = x;
		planet[currentRowMatrix][currentColumnMatrix] = 2;
		currentDirection = direction;
	}
	
	private void singleCommand(String c) {
		if(c.compareTo("r") == 0) {
			turnRight();
		} else if(c.compareTo("l") == 0) {
			turnLeft();
		} else if(c.compareTo("f") == 0) {
			moveForward();
		} else if(c.compareTo("b") == 0) {
			moveBack();
		}
	}
	
	private void moreCommands(String commands) {
		for(int i = 0; i < commands.length(); i++) {
			singleCommand(String.valueOf(commands.charAt(i)));
		}
	}
	
	private void goingOverTopMargin() {
		planet[currentRowMatrix][currentColumnMatrix] = 0;
		currentYplanet = 0;
		currentRowMatrix = planet.length - 1;
		planet[currentRowMatrix][currentColumnMatrix] = 2;
	}
	
	private void goingOverBottomMargin() {
		planet[currentRowMatrix][currentColumnMatrix] = 0;
		currentYplanet = planet[0].length - 1;
		currentRowMatrix = 0;
		planet[currentRowMatrix][currentColumnMatrix] = 2;
	}
	
	private void goingOverLeftMargin() {
		planet[currentRowMatrix][currentColumnMatrix] = 0;
		currentXplanet = planet.length - 1;
		currentColumnMatrix = planet[0].length - 1;
		planet[currentRowMatrix][currentColumnMatrix] = 2;
	}
	
	private void goingOverRightMargin() {
		planet[currentRowMatrix][currentColumnMatrix] = 0;
		currentXplanet = 0;
		currentColumnMatrix = 0;
		planet[currentRowMatrix][currentColumnMatrix] = 2;
	}

	private void directionNorthMoveBackOperations() {
		if(currentRowMatrix == (planet.length - 1)) {
			goingOverBottomMargin();
		} else {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentYplanet--;
			currentRowMatrix++;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		}
	}
	
	private void directionEastMoveBackOperations() {
		if(currentColumnMatrix == 0) {
			goingOverLeftMargin();
		} else {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentXplanet--;
			currentColumnMatrix--;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		}
	}
	
	private void directionSouthMoveBackOperations() {
		if(currentRowMatrix == 0) {
			goingOverTopMargin();
		} else {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentYplanet++;
			currentRowMatrix--;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		}
	}
	
	private void directionWestMoveBackOperations() {
		if(currentColumnMatrix == (planet[0].length - 1)) {
			goingOverRightMargin();
		} else {
			planet[currentRowMatrix][currentColumnMatrix] = 0;
			currentXplanet++;
			currentColumnMatrix++;
			planet[currentRowMatrix][currentColumnMatrix] = 2;
		}
	}
}
